import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard/dashboard.component';
import {SignInComponent} from './authentication/components/sign-in/sign-in.component';
import {SignUpComponent} from './authentication/components/sign-up/sign-up.component';
import {ForgotPasswordComponent} from './authentication/components/forgot-password/forgot-password.component';
import {VerifyEmailComponent} from './authentication/components/verify-email/verify-email.component';
import {AuthGuard} from './authentication/guard/auth.guard';
import {SecureInnerPagesGuard} from './authentication/guard/secure-inner-pages.guard.ts.guard';


const routes: Routes = [
  {path: '', redirectTo: '/sign-in', pathMatch: 'full'},
  {path: 'dashboard',  pathMatch: 'full', component: DashboardComponent, canActivate: [AuthGuard]},
  {path: 'sign-in', pathMatch: 'full', component: SignInComponent, canActivate: [SecureInnerPagesGuard]},
  {path: 'register-user', pathMatch: 'full', component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
  {path: 'forgot-password', pathMatch: 'full', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard]},
  {path: 'verify-email-address', pathMatch: 'full', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
