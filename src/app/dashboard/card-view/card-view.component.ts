import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Card, RootService, Status} from '../../service/root.service';
import {NotificationService} from '../../notifications/notification.service';
import {first} from 'rxjs/operators';
import {PrintService} from '../../service/print.service';
import {ConfirmationComponent} from '../../layout/confirmation/confirmation.component';

@Component({
  selector: 'app-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.scss']
})
export class CardViewComponent implements OnInit {

  public card: Card;
  public tableData = [];

  public displayedColumns: string[] = ['property', 'value'];

  constructor(
    private dialog: MatDialog,
    private dialogRef: MatDialogRef<CardViewComponent>,
    @Inject(MAT_DIALOG_DATA) public data: string,
    private rootService: RootService,
    private notification: NotificationService,
    private printService: PrintService) {
  }

  ngOnInit(): void {
    this.rootService.getCard(this.data).pipe(first()).subscribe(card => {
      this.card = card;
      this.generateTableData();
    }, error1 => this.notification.message(`Error while loading data for card with id: ${this.data}`));
  }


  generateTableData(): void {
    this.tableData.push({property: 'Id', value: this.card.id});
    this.tableData.push({property: 'Full Name', value: this.card.fullName});
    this.tableData.push({property: 'Phone Number', value: this.card.phoneNumber});
    this.tableData.push({property: 'Device Model', value: this.card.model});
    this.tableData.push({property: 'Serial Number', value: this.card.serialNumber});
    this.tableData.push({property: 'Set', value: this.card.set});
    this.tableData.push({property: 'Malfunction', value: this.card.malfunction});
    this.tableData.push({property: 'Notes', value: this.card.notes});
    this.tableData.push({property: 'Status', value: Status[this.card.status]});
    this.tableData.push({property: 'Creation Date', value: this.card.date.toDate().toLocaleString()});
    this.tableData.push({property: 'Last Updating Date', value: this.card.lastUpdate.toDate().toLocaleString()});
  }

  saveComment(comment): void {
    this.rootService.updateCardComment(this.card.id, comment).then(() => {
      this.notification.message('Data was saved!');
      this.dialogRef.close();
    }, () => {
      this.notification.message('Error saving data');
    });
  }

  printBlank(): void {
    this.printService.printCardBlank(this.card);
  }

  removeCard(): void {
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      data: `Are you sure that you want to remove card with id: ${this.card.id} ?`
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.rootService.removeCard(this.card.id).then(() => {
          this.notification.message(`Card with id: ${this.card.id} was removed`);
          this.dialogRef.close();
        }, () => this.notification.message(`Error removing card with id: ${this.card.id}`));
      }
    });
  }

}
