import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Card, RootService, Status} from '../../service/root.service';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {MatDialog} from '@angular/material/dialog';
import {AddCardComponent} from '../add-card/add-card.component';
import {NotificationService} from '../../notifications/notification.service';
import {Subscription} from 'rxjs';
import {firestore} from 'firebase';
import {MatDatepickerInputEvent} from '@angular/material/datepicker';
import {CardViewComponent} from '../card-view/card-view.component';
import {PrintService} from '../../service/print.service';
import {ConfirmationComponent} from '../../layout/confirmation/confirmation.component';
import {DOCUMENT} from '@angular/common';
import {AuthenticationService} from '../../authentication/service/authentication.service';
import {User} from '../../authentication/authentication.module';
import Timestamp = firestore.Timestamp;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  public filter = '';
  public isLoadingCards = false;


  constructor(@Inject(DOCUMENT) private document: Document,
              private dialog: MatDialog,
              private rootService: RootService,
              private notifications: NotificationService,
              private notification: NotificationService,
              private printService: PrintService,
              private authService: AuthenticationService) {
  }

  public newCards: Array<Card> = [];
  public inProgressCards: Array<Card> = [];
  public waitingCards: Array<Card> = [];
  public doneSuccessCards: Array<Card> = [];
  public doneFailureCards: Array<Card> = [];

  private cardSubscription: Subscription;

  ngOnInit(): void {
    this.getCards();
  }

  getCards(startDate?: Date): void {
    this.isLoadingCards = true;
    startDate = startDate ? startDate : this.getDefaultStartQueryDate();
    this.cardSubscription = this.rootService.getCards(Timestamp.fromDate(startDate)).subscribe(cards => {
      this.clearCardList();
      cards.forEach(card => {
        switch (card.status) {
          case Status.NEW:
            this.newCards.push(card);
            break;
          case Status.IN_PROGRESS:
            this.inProgressCards.push(card);
            break;
          case Status.WAITING:
            this.waitingCards.push(card);
            break;
          case Status.DONE_FAILURE:
            this.doneFailureCards.push(card);
            break;
          case Status.DONE_SUCCESS:
            this.doneSuccessCards.push(card);
            break;
        }
      });
      this.isLoadingCards = false;
    }, error1 => this.notifications.message('Error while loading cards'));
  }

  clearCardList(): void {
    this.newCards = [];
    this.inProgressCards = [];
    this.waitingCards = [];
    this.doneSuccessCards = [];
    this.doneFailureCards = [];
  }

  drop(event: CdkDragDrop<Card[]>, statusIndex: number) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      this.updateCardStatus(event.container.data[0].id, statusIndex);
    }
  }

  openAddCardDialog(): void {
    const dialogRef = this.dialog.open(AddCardComponent, {
      width: '90vw',
      maxHeight: '95vh'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.saveNewCard(result);
      }
    });
  }


  saveNewCard(card: Card): void {
    this.rootService.addNewCard(card).then(() => {
      this.notifications.message(`Card with id: ${card.id} was added!`);
    }).catch(() => {
      this.notifications.message(`Error adding card with id: ${card.id}!`);
    });
  }

  updateCardStatus(cardId: string, statusIndex: number): void {
    const statusByIndex = this.getStatusByIndex(statusIndex);
    this.rootService.updateCardStatus(cardId, statusByIndex).then(() => {
      // this.notifications.message(`For card with id: ${cardId} status was changed to ${Status[statusByIndex]}`);
    }).catch(() => {
      this.notifications.message(`Error changing status for card with id: ${cardId}`);

    });
  }

  getFilteredCards(statusIndex: number): Array<Card> {
    const status = this.getStatusByIndex(statusIndex);
    let cardsToFilter: Array<Card> = [];
    switch (status) {
      case Status.NEW:
        cardsToFilter = this.newCards;
        break;
      case Status.IN_PROGRESS:
        cardsToFilter = this.inProgressCards;
        break;
      case Status.WAITING:
        cardsToFilter = this.waitingCards;
        break;
      case Status.DONE_SUCCESS:
        cardsToFilter = this.doneSuccessCards;
        break;
      case Status.DONE_FAILURE:
        cardsToFilter = this.doneFailureCards;
        break;
    }

    return cardsToFilter.filter(card => {
      return card.fullName.includes(this.filter) ||
        card.phoneNumber.includes(this.filter) ||
        card.id.includes(this.filter) ||
        card.serialNumber.includes(this.filter);
    });
  }


  getStatusByIndex(index: number): Status {
    switch (index) {
      case 0:
        return Status.NEW;
        break;
      case 1:
        return Status.IN_PROGRESS;
        break;
      case 2:
        return Status.WAITING;
        break;
      case 3:
        return Status.DONE_SUCCESS;
        break;
      case 4:
        return Status.DONE_FAILURE;
        break;
    }
  }

  startDateChanged(event: MatDatepickerInputEvent<Date>): void {
    this.getCards(event.value);
  }

  getDefaultStartQueryDate(): Date {
    const date = new Date();
    date.setMonth(date.getMonth() - 3);
    return date;
  }

  openCardViewDialog(cardId: string): void {
    this.dialog.open(CardViewComponent, {
      width: '90vw',
      maxHeight: '95vh',
      data: cardId
    });
  }

  appendStatus(next: boolean, card: Card): void {
    if(next && card.status === Status.DONE_FAILURE || !next && card.status === Status.NEW) {
      return;
    }
    const index = next ? card.status + 1 : card.status - 1;
    this.updateCardStatus(card.id, index);
    console.log(next);
  }

  removeCard(card: Card): void {
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      data: `Are you sure that you want to remove card with id: ${card.id} ?`
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.rootService.removeCard(card.id).then(() => {
          this.notification.message(`Card with id: ${card.id} was removed`);
        }, () => this.notification.message(`Error removing card with id: ${card.id}`));
      }
    });
  }

  printBlank(card: Card): void {
    this.printService.printCardBlank(card);
  }

  assignMe(card: Card): void{
    const currentUser: User = this.authService.currentUser();

    const dialogRef = this.dialog.open(ConfirmationComponent, {
      data: `Are you sure that you want to assign yourself to card with id: ${card.id} ?`
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.rootService.updateAssignedUserdEmail(card.id, currentUser.email).then(() => {
          this.notifications.message(`${currentUser.displayName}! You are assigned to card: ${card.id}`);
        });
      }
    });
  }

  cardListsToTop(): void {
    Array.from(this.document.scrollingElement.getElementsByClassName('cdk-drop-list')).forEach((el) => {
      el.scrollTo({
        top: 0,
        behavior: 'smooth'
      })
    });

  }

  ngOnDestroy(): void {
    this.cardSubscription.unsubscribe();
  }

}
