import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Card, Status} from '../../service/root.service';
import {generate} from 'shortid';
import {MatDialogRef} from '@angular/material/dialog';
import {firestore} from 'firebase';
import Timestamp = firestore.Timestamp;

@Component({
  selector: 'app-add-card',
  templateUrl: './add-card.component.html',
  styleUrls: ['./add-card.component.scss']
})
export class AddCardComponent implements OnInit {

  public cardForm: FormGroup;

  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<AddCardComponent>) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

  buildForm(): void {
    this.cardForm = this.fb.group({
      fullName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required]],
      model: ['', [Validators.required]],
      serialNumber: ['', [Validators.required]],
      malfunction: ['', [Validators.required]],
      note: [''],
      comment: ['']
    });
  }

  isFormValid(additionalValidForm: boolean): boolean {
    return additionalValidForm && this.cardForm.valid;

  }


  buildSetString(noteBook: boolean, akb: boolean, chargeCable: boolean, chargeBlock: boolean,
                 packet: boolean, pc: boolean, tablet: boolean, smartPhone: boolean): string {
    const setItems: Array<string> = [];
    if (noteBook) {
      setItems.push('Ноутбук');
    }
    if (akb) {
      setItems.push('АКБ');
    }
    if (chargeCable) {
      setItems.push('Кабель Зарядки');
    }
    if (chargeBlock) {
      setItems.push('Кабель Питания');
    }
    if (packet) {
      setItems.push('Сумка');
    }
    if (pc) {
      setItems.push('PC');
    }
    if (tablet) {
      setItems.push('Планшет');
    }
    if (smartPhone) {
      setItems.push('Смартфон');
    }
    return setItems.join(', ');
  }

  submit(set: string): void {
    if (!this.cardForm.valid || !set) {
      return;
    }

    const currentTime = Timestamp.now();
    const card: Card = {
      id: generate(),
      fullName: this.cardForm.controls.fullName.value,
      phoneNumber: this.cardForm.controls.phoneNumber.value,
      model: this.cardForm.controls.model.value,
      serialNumber: this.cardForm.controls.serialNumber.value,
      malfunction: this.cardForm.controls.malfunction.value,
      notes: this.cardForm.controls.note.value,
      comment: this.cardForm.controls.comment.value,
      status: Status.NEW,
      date: currentTime,
      lastUpdate: currentTime,
      set,
      price: 0,
      assignedUserEmail: ''
    };

    this.dialogRef.close(card);

  }

}
