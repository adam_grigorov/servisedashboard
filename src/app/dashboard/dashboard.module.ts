import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FlexLayoutModule, FlexModule} from '@angular/flex-layout';
import {MatDividerModule} from '@angular/material/divider';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDialogModule} from '@angular/material/dialog';
import {AddCardComponent} from './add-card/add-card.component';
import {MatRadioModule} from '@angular/material/radio';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDatepickerIntl, MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {CardViewComponent} from './card-view/card-view.component';
import {MatTableModule} from '@angular/material/table';
import {CardComponent} from './card/card.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {ClipboardModule} from '@angular/cdk/clipboard';


@NgModule({
  declarations: [DashboardComponent, AddCardComponent, CardViewComponent, CardComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FlexModule,
    MatDividerModule,
    MatCardModule,
    DragDropModule,
    MatButtonModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    MatRadioModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    ClipboardModule
  ],
  entryComponents: [AddCardComponent, CardViewComponent],
  providers: [MatDatepickerIntl]
})
export class DashboardModule {
}
