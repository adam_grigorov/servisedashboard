import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Card} from '../../service/root.service';
import {NotificationService} from '../../notifications/notification.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input('card')
  public card: Card;

  @Output('appendStatus')
  public appendStatus: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output('removeCard')
  public removeCard: EventEmitter<Card> = new EventEmitter<Card>();

  @Output('print')
  public print: EventEmitter<Card> = new EventEmitter<Card>();

  @Output('assignMe')
  public assignMe: EventEmitter<Card> = new EventEmitter<Card>();

  constructor(private notifications: NotificationService) {
  }

  ngOnInit(): void {
  }

  copy(data: string): void{
    this.notifications.message(`Data copied: ${data}`);
  }

}
