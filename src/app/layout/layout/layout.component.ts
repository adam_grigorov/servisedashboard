import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {UserViewComponent} from '../../authentication/components/user-view/user-view.component';
import {AuthenticationService} from '../../authentication/service/authentication.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(public dialog: MatDialog,
              public authService: AuthenticationService) { }

  ngOnInit(): void {
  }

  openUserView() {
    this.dialog.open(UserViewComponent);
  }

}
