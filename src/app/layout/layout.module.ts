import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutComponent} from './layout/layout.component';
import {RouterModule} from '@angular/router';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {DashboardModule} from '../dashboard/dashboard.module';
import {ConfirmationComponent} from './confirmation/confirmation.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [LayoutComponent, ConfirmationComponent],
  imports: [
    CommonModule,
    RouterModule,
    MatToolbarModule,
    FlexLayoutModule,
    DashboardModule,
    MatButtonModule,
    MatIconModule
  ],
  entryComponents: [ConfirmationComponent],
  exports: [LayoutComponent]
})
export class LayoutModule {
}
