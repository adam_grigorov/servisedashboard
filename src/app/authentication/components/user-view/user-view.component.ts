import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../service/authentication.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.scss']
})
export class UserViewComponent implements OnInit {

  constructor(public authService: AuthenticationService,
              public dialogRef: MatDialogRef<UserViewComponent>) { }

  ngOnInit(): void {
  }

  signOut() {
    this.authService.signOut();
    this.dialogRef.close();
  }

}
