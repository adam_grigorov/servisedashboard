import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../service/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.scss']
})
export class VerifyEmailComponent implements OnInit {

  constructor(public authService: AuthenticationService,
              private router: Router) { }

  ngOnInit(): void {
    if(!this.authService.userData) {
      this.router.navigate(['sign-in']);
    }
  }

}
