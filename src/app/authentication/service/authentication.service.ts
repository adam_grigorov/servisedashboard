import {Injectable, NgZone} from '@angular/core';
import {auth} from 'firebase/app';
import {AngularFirestore, AngularFirestoreDocument} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {User} from '../authentication.module';
import {NotificationService} from '../../notifications/notification.service';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  userData: any; // Save logged in user data
  userCollection = 'user';

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    public notifications: NotificationService
  ) {
    /* Saving user data in localstorage when
    logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
        this.router.navigate(['dashboard']);
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
        this.router.navigate(['sign-in']);
      }
    });
  }

  // Sign in with email/password
  signIn(email, password) {
    this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.setUserData(result.user);
      }).catch((error) => {
        this.notifications.message(error.message);
      });
  }

  // Sign up with email/password
  signUp(email, password) {
    this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign
        up and returns promise */
        this.sendVerificationMail();
        this.setUserData(result.user);
      }).catch((error) => {
        this.notifications.message(error.message);
      });
  }

  // Sign in with Google
  googleAuth() {
    return this.authLogin(new auth.GoogleAuthProvider());
  }

  // Auth logic to run auth providers
  authLogin(provider) {
    this.afAuth.signInWithPopup(provider)
      .then((result) => {
        this.ngZone.run(() => {
          this.router.navigate(['dashboard']);
        });
        this.setUserData(result.user);
      }).catch((error) => {
        this.notifications.message(error);
      });
  }

  /* Setting up user data when sign in with username/password,
  sign up with username/password and sign in with social auth
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  setUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`${this.userCollection}/${user.uid}`);
    const userData: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified
    };
    return userRef.set(userData, {
      merge: true
    });
  }

  getUserList(): Observable<Array<User>> {
    return this.afs.collection<User>(this.userCollection).valueChanges();
  }

  currentUser(): User {
    return JSON.parse(localStorage.getItem('user'));
  }

  // Sign out
  signOut() {
    return this.afAuth.signOut().then(() => {
      localStorage.removeItem('user');
      this.router.navigate(['sign-in']);
    });
  }

  sendVerificationMail(resend?: boolean) {
    this.afAuth.currentUser.then(currentUser => {
      currentUser.sendEmailVerification({url: environment.host +'/sign-in'}).then(() => {
        this.router.navigate(['verify-email-address']);
        if(resend) {
          this.notifications.message('Verification email was sent!');
        }
      }).catch(error => {
        this.notifications.message(error);
      })
    });
  }

  // Reset Forggot password
  forgotPassword(passwordResetEmail) {
    this.afAuth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        this.notifications.message('Password reset email sent, check your inbox.');
      }).catch((error) => {
        this.notifications.message(error)
      })
  }

  // Returns true when user is looged in and email is verified
  public get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null && user.emailVerified !== false);
  }

}
