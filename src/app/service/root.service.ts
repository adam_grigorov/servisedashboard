import {Injectable} from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';
import {Observable} from 'rxjs';
import {firestore} from 'firebase';
import Timestamp = firestore.Timestamp;

@Injectable({
  providedIn: 'root'
})
export class RootService {

  private cardCollection = 'card';

  constructor(private fireStore: AngularFirestore) {
  }

  public getCards(startDate: Timestamp): Observable<Array<Card>> {
    return this.fireStore.collection<Card>(this.cardCollection, ref => {
      return ref.where('lastUpdate', '>=', startDate).orderBy('lastUpdate', 'desc');
    }).valueChanges();
  }

  public addNewCard(card: Card): Promise<void> {
    return this.fireStore.collection<Card>(this.cardCollection).doc<Card>(card.id).set(card);
  }

  public updateCardStatus(id: string, status: Status): Promise<void> {
    return this.fireStore.collection<Card>(this.cardCollection).doc<Card>(id).update({status, lastUpdate: Timestamp.now()});
  }

  public updateCardComment(id: string, comment: string): Promise<void> {
    return this.fireStore.collection<Card>(this.cardCollection).doc<Card>(id).update({comment, lastUpdate: Timestamp.now()});
  }

  public updatePrice(id: string, price: number): Promise<void> {
    return this.fireStore.collection<Card>(this.cardCollection).doc<Card>(id).update({price, lastUpdate: Timestamp.now()});

  }

  public updateAssignedUserdEmail(id: string, assignedUserId): Promise<void> {
    return this.fireStore.collection<Card>(this.cardCollection).doc<Card>(id)
      .update({assignedUserEmail: assignedUserId, lastUpdate: Timestamp.now()});

  }

  public getCard(cardId: string): Observable<Card>  {
    return this.fireStore.collection<Card>(this.cardCollection).doc<Card>(cardId).valueChanges();
  }

  public removeCard(cardId: string): Promise<void> {
    return this.fireStore.collection<Card>(this.cardCollection).doc<Card>(cardId).delete();
  }

}


export interface Card {
  id: string;
  fullName: string;
  phoneNumber: string;
  model: string;
  serialNumber: string;
  set: string;
  malfunction: string;
  notes: string;
  comment: string;
  status: Status;
  date: Timestamp;
  lastUpdate: Timestamp;
  price: number;
  assignedUserEmail: string;
}

export enum Status {
  NEW, IN_PROGRESS, WAITING, DONE_SUCCESS, DONE_FAILURE
}
