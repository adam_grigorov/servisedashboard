import {Injectable} from '@angular/core';
import {Card} from './root.service';

@Injectable({
  providedIn: 'root'
})
export class PrintService {

  private address = 'ул. Авиаконструктора Игоря Сикорского, 4Б';
  private phone = '+380994045459, +380684828828';

  private currentDate = new Date().toLocaleDateString();

  constructor() {
  }

  public printCardBlank(card: Card): void {
    let popupWindow;
    popupWindow = window.open('', '_blank', 'width=600,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    popupWindow.document.open();
    popupWindow.document.write(`<html>
<head>
  <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <style>
      body > *{
        font-family: 'Roboto', sans-serif;
        font-size: 0.8em !important;
      }
  </style>
</head>
<body onload="window.print()">
<div>
  <div style="display: inline-block">
    <img style="max-height: 60px" src="../../assets/amarket-service.png" alt="">
  </div>
  <div style="display: inline-block">
    <table>
      <tr>
        <td>адрес:</td>
        <td>${this.address}</td>
      </tr>
      <tr>
        <td>тел.:</td>
        <td>${this.phone}</td>
      </tr>
    </table>
  </div>
</div>
<table border="2px solid black" style="width: 100%; border-collapse: collapse">
  <tr>
    <td width="30%" style="font-weight: bold">ID квитанции</td>
    <td>${card.id}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">ФИО клиента</td>
    <td>${card.fullName}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">Номер телефона</td>
    <td>${card.phoneNumber}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">Модель</td>
    <td>${card.model}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">Серийный номер</td>
    <td>${card.serialNumber}</td>
  </tr>
  <tr>
    <td style="font-weight: bold" width="30%">Комплектация</td>
    <td>${card.set}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold; text-align: center">Заявленная неисправность</td>
  </tr>
  <tr style="height: 18px">
    <td colspan="2">${card.malfunction}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold; text-align: center">Примечание</td>
  </tr>
  <tr style="height: 18px">
    <td colspan="2">${card.notes}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold; text-align: center">Условия ремонта</td>
  </tr>
  <tr>
    <td colspan="2">
      <ol>
        <li>Стоимость диагностики 250грн. Оплачивается клиентом в случае отказа от ремонта.</li>
        <li>В случаи если не удается определить неисправность в течение 14 рабочих дней,
          устройство выдается клиенту без оплаты диагностики.
        </li>
        <li>Срок ремонта ограничивается индивидуально по каждому устройству.</li>
        <li>Ремонт техники со следами влаги, коррозии, ударов, падений — выполняется без гарантийных обязательств
          (БЕЗ ГАРАНТИИ на выполненный ремонт).
        </li>
        <li>Диагностика производится только заявленных клиентом неисправностей, все дополнительные неисправности
          найденные в ходе диагностики согласуются с клиентом отдельно.
        </li>
        <li>Исполнитель не производит не каких операций с информацией клиента на его носителях, не устанавливает и не
          удаляет
          пользовательские программы либо вирусы, и не вносит изменений в пользовательские настройки.
        </li>
        <li>Устройство принимается в собранном виде и без проверки внутренних повреждений. Клиент понимает, что все
          неисправности и внутренние повреждения, которые могут быть обнаружены в аппарате в ходе диагностики, возникли
          до приёма
          устройства в ремонт.
        </li>
      </ol>
    </td>
  </tr>
</table>
<table style="width: 100%; border-spacing: 20px 0; margin-top: 10px;">
  <tr>
    <td style="width: 70%; text-align: end; font-weight: bold;">С перечисленным выше и условиями ремонта ознакомлен</td>
    <td style="border-bottom: 2px black solid; text-align: center">${this.currentDate}</td>
    <td style="border-bottom: 2px black solid"></td>
  </tr>
  <tr>
    <td style="width: 70%"></td>
    <td style="text-align: center">дата</td>
    <td style="text-align: center">подпись</td>
  </tr>
  <tr style="height: 40px">
    <td style="width: 70%; text-align: end; vertical-align: bottom; font-weight: bold;">Устройство принято</td>
    <td colspan="2" style="border-bottom: 2px black solid"></td>
  </tr>
  <tr>
    <td style="width: 70%"></td>
    <td colspan="2" style="text-align: center">подпись / штамп</td>
  </tr>
</table>
<div style="border-bottom: 2px black dashed; width: 100%; margin: 25px 0 25px 0;"></div>
<div>
  <div style="display: inline-block">
    <img style="max-height: 60px" src="../../assets/amarket-service.png" alt="">
  </div>
  <div style="display: inline-block">
    <table>
      <tr>
        <td>адрес:</td>
        <td>${this.address}</td>
      </tr>
      <tr>
        <td>тел.:</td>
        <td>${this.phone}</td>
      </tr>
    </table>
  </div>
</div>
<table border="2px solid black" style="width: 100%; border-collapse: collapse">
  <tr>
    <td width="30%" style="font-weight: bold">ID квитанции</td>
    <td>${card.id}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">ФИО клиента</td>
    <td>${card.fullName}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">Номер телефона</td>
    <td>${card.phoneNumber}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">Мдель</td>
    <td>${card.model}</td>
  </tr>
  <tr>
    <td width="30%" style="font-weight: bold">Серийный номер</td>
    <td>${card.serialNumber}</td>
  </tr>
  <tr>
    <td style="font-weight: bold" width="30%">Комплектация</td>
    <td>${card.set}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold; text-align: center">Заявленная неисправность</td>
  </tr>
  <tr style="height: 18px">
    <td colspan="2">${card.malfunction}</td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight: bold; text-align: center">Примечание</td>
  </tr>
  <tr style="height: 18px">
    <td colspan="2">${card.notes}</td>
  </tr>
</table>
<table style="width: 100%; border-spacing: 20px 0; margin-top: 10px;">
  <tr>
    <td style="width: 70%; text-align: end; font-weight: bold;">Устройство получено, притензий не имею</td>
    <td style="border-bottom: 2px black solid;"></td>
    <td style="border-bottom: 2px black solid;"></td>
  </tr>
  <tr>
    <td style="width: 70%"></td>
    <td style="text-align: center; width: 15%;">дата</td>
    <td style="text-align: center; width: 15%;">подпись</td>
  </tr>
</table>
</body>
</html>
`);
    popupWindow.document.close();
  }

}
