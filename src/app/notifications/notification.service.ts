import {Injectable} from '@angular/core';
import {NotificationComponent} from './notification/notification.component';
import {MatSnackBar, MatSnackBarConfig} from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    private config: MatSnackBarConfig = {
        duration: 5000,
        horizontalPosition: 'right',
        verticalPosition: 'top'
    };

    constructor(private snackBar: MatSnackBar) {
    }

    message(message: string) {
        const config = this.config;
        config.data = {message};
        this.snackBar.openFromComponent(NotificationComponent, config);
    }


}
